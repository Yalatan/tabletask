import { Component, OnInit } from '@angular/core';
import * as faker from 'faker';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  fruits = [{name: 'fruits'}, {name: 'apple'}, {name: 'plum'}, {name: 'peach'}];
  selectedFruit = this.fruits[0];
  fruitsDataList = [];
  generalGroupData = [];
  details = [];
  chosen = '';

  ngOnInit(): void {
    for (let i = 1; i < 100; i++) {
      const nameList = faker.random.arrayElement(['apple', 'plum', 'peach']);
      const colorList = faker.random.arrayElement(['green', 'yellow', 'red']);
      const revenueList = faker.random.number(20);
      const fakeFruit = {
        name: nameList,
        price: revenueList,
        color: colorList
      };
      this.fruitsDataList.push(fakeFruit);
    }

    const group = this.fruitsDataList.reduce((acc, item) => {
      if (!acc[item.name]) {
        acc[item.name] = [];
      }
      acc[item.name].push(item);
      return acc;
    }, {});

    Object.keys(group).forEach(e => {
      const sum = group[e].reduce((a, {price}) => a + price, 0);
      const obj = {
        name: e,
        leadsCount: group[e].length,
        revenue: sum,
        rpl: parseFloat(String(sum / group[e].length)).toFixed(2)
      };
      this.generalGroupData.push(obj);
    });
  }

  chooseFruit(fruit): void {
    this.details = [];
    this.chosen = fruit.name;
    const fruitFound = this.fruitsDataList.filter(el => el.name === fruit.name);

    const group = fruitFound.reduce((acc, item) => {
      if (!acc[item.color]) {
        acc[item.color] = [];
      }
      acc[item.color].push(item);
      return acc;
    }, {});

    Object.keys(group).forEach(e => {
      const sum = group[e].reduce((a, {price}) => a + price, 0);
      const obj = {
        color: e,
        leadsCount: group[e].length,
        revenue: sum,
        rpl: parseFloat(String(sum / group[e].length)).toFixed(2)
      };
      this.details.push(obj);
    });
  }
}
